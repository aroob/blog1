u:Gem::Specification�[I"
2.2.2:ETi	I"test-unit; TU:Gem::Version[I"
3.0.1; TIu:	Time���    :	zoneI"UTC; TI"Ftest-unit - Improved version of Test::Unit bundled in Ruby 1.8.x.; TU:Gem::Requirement[[[I">=; TU;[I"0; TU;	[[[I">=; TU;[I"0; TI"	ruby; T[o:Gem::Dependency
:
@nameI"power_assert; T:@requirementU;	[[[I">=; TU;[I"0; T:
@type:runtime:@prereleaseF:@version_requirementsU;	[[[I">=; TU;[I"0; To;

;I"bundler; T;U;	[[[I">=; TU;[I"0; T;:development;F;U;	[[[I">=; TU;[I"0; To;

;I"	rake; T;U;	[[[I">=; TU;[I"0; T;;;F;U;	[[[I">=; TU;[I"0; To;

;I"	yard; T;U;	[[[I">=; TU;[I"0; T;;;F;U;	[[[I">=; TU;[I"0; To;

;I"kramdown; T;U;	[[[I">=; TU;[I"0; T;;;F;U;	[[[I">=; TU;[I"0; To;

;I"packnga; T;U;	[[[I">=; TU;[I"0; T;;;F;U;	[[[I">=; TU;[I"0; T0[I"kou@cozmixng.org; TI"yoshihara@clear-code.com; T[I"Kouhei Sutou; TI"Haruka Yoshihara; TI"�Ruby 1.9.x bundles minitest not Test::Unit. Test::Unit
bundled in Ruby 1.8.x had not been improved but unbundled
Test::Unit (test-unit) is improved actively.
; TI"'http://rubygems.org/gems/test-unit; TT@[I"	Ruby; TI"	PSFL; T{ 